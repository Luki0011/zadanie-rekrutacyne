# ZADANIE REKRUTACYJNE


### Uruchomienie projektu

```sh 
docker-compose up
```

strona działa pod adresem:

```sh 
localhost:8002
```

Znajduje sie tutaj główna część zadania, mail wysyłany jest do messangera.
W przypadku błędu, próby ponowienia ustawione są 2 pierwsza po 60, druga po 180 sekundach od wysłania.
W przypadku niepowodzenia spada na failed.


Odpalenie messangera:
```
php bin/console messenger:consume send_mail
```

Obsługa notyfikacji pod adresem:

```sh 
localhost:8002/notification
```

Domyślne dostepy do bazy w .env


Do uzupełnienia:
```sh 
MAILER_DSN= konfig skrzynki
SENDER_MAIL= mail z którego wysyłamy formularz
DESTINATION_MAIL= mail na który ma wpadać formularz kontaktowy
```
