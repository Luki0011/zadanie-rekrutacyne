<?php


namespace App\Message;


class Mail
{
    private string $name;

    private string $email;

    private string $content;

    public function __construct(string $name, string $email, string $content)
    {
        $this->name = $name;
        $this->email = $email;
        $this->content = $content;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
