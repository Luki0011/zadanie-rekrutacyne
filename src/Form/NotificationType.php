<?php


namespace App\Form;


use App\Entity\Contact;
use App\Entity\Notification;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NotificationType extends AbstractType
{

    private User $user;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->user = $options['user'];
        $builder
            ->add('user', EntityType::class, [
                'class' => User::class,
                'choice_label' => 'name',
                'disabled' => true,
                'data' => $this->user,
                'mapped' => false,

            ])
            ->add('contact', EntityType::class, [
                'class' => Contact::class,
                'choice_label' => 'email',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->where('c.user = :user')
                        ->setParameter('user', $this->user);
                }
            ])
            ->add('content', TextareaType::class)
            ->add('send', SubmitType::class, [ 'label' => 'Dodaj']);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired([
            'user',
        ]);

        $resolver->setDefaults([
            'data_class' => Notification::class,
            'empty_data' => function (FormInterface $form) {
                return new Notification(
                    $form->get('content')->getData(),
                    $form->get('contact')->getData(),
                    1
                );
            }
        ]);
    }



}
