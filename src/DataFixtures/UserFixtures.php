<?php


namespace App\DataFixtures;


use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    const NAMES = [
        'Ala',
        'Ola',
        'Adam',
        'Kasia',
    ];

    public function load(ObjectManager $manager)
    {
        foreach (self::NAMES as $name) {
            $manager->persist(new User($name));
            $manager->flush();
        }
    }
}
