<?php


namespace App\MessageHandler;


use App\Message\Mail;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class MailHandler implements MessageHandlerInterface
{
    private MailerInterface $mailer;

    private ParameterBagInterface $parameterBag;

    private LoggerInterface $logger;

    public function __invoke(Mail $mail)
    {
        $email = (new TemplatedEmail())
            ->from($this->parameterBag->get('sender_mail'))
            ->to($mail->getEmail())
            ->htmlTemplate('email/contact.html.twig')
            ->context([
                'name' => $mail->getName(),
                'content' => $mail->getContent()
            ]);

            $this->mailer->send($email);
    }


    public function __construct(MailerInterface $mailer, ParameterBagInterface $parameterBag, LoggerInterface $logger)
    {
        $this->mailer = $mailer;
        $this->parameterBag = $parameterBag;
        $this->logger = $logger;
    }

}
