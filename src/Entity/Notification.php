<?php


namespace App\Entity;


class Notification
{
    private int $id;
    private int $status;
    private string $content;
    private Contact $contact;

    public function __construct(string $content, Contact $contact, int $status = 1)
    {
        $this->content = $content;
        $this->contact = $contact;
        $this->status = $status;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getContact(): Contact
    {
        return $this->contact;
    }

    public function getContent(): string
    {
        return $this->content;
    }
}
