<?php


namespace App\Entity;


class Contact
{
    private int $id;
    private string $name;
    private string $email;
    private User $user;

    public function __construct(string $name, string $email, User $user)
    {
        $this->name = $name;
        $this->email = $email;
        $this->user = $user;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
