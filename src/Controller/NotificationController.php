<?php


namespace App\Controller;

use App\Entity\User;
use App\Form\ContactType;
use App\Form\NotificationType;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class NotificationController extends AbstractController
{

    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/notification", name="notification")
     */
    public function index(Request $request, MessageBusInterface $bus)
    {
        return $this->render('notification.html.twig',[
            'users' => $this->em->getRepository(User::class)->findAll(),
            ]);
    }

    /**
     * @Route("/notification/add/{type}", name="notification_add")
     */
    public function addUser(Request $request)
    {
        $type = $request->get('type');

        if("user" !== $type && "contact" !== $type) {
            throw new NotFoundHttpException();
        }

        $form = ('user' === $type) ? $this->createForm(UserType::class) : $this->createForm(ContactType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $this->em->persist($form->getData());
            $this->em->flush();

            return $this->redirectToRoute('notification');
        }

        return $this->render('notification-add.html.twig', [
            'form' =>$form->createView()
        ]);
    }

    /**
     * @Route("/notification/send/{user_id}", name="notification_send")
     */
    public function sendNotification(Request $request)
    {
        $form = $this->createForm(NotificationType::class,null, ['user' => $this->em->getRepository(User::class)->find($request->get('user_id'))]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($form->getData());
            $this->em->flush();
        }

        return $this->render('notification-send.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/remove_user/{id}", name="remove_user")
     */
    public function removeUser(Request $request)
    {
        $user = $this->em->getRepository(User::class)->find($request->get('id'));
        $this->em->remove($user);
        $this->em->flush();

        return $this->redirectToRoute('notification');
    }


}
