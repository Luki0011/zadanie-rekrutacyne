<?php


namespace App\Controller;


use App\Form\MailType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(Request $request, MessageBusInterface $bus)
    {
        $form = $this->createForm(MailType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $bus->dispatch($form->getData());
        }

        return $this->render('main.html.twig', [
            'form' => $form->createView()
        ]);
    }

}
